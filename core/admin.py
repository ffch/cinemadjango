from django.contrib import admin
from .models import Pelicula, Profile
# Register your models here.

class PeliculaAdmin(admin.ModelAdmin):
    list_display=['titulo', 'anio', 'duracion', 'pais', 'director']
    search_fields=['titulo', 'anio', 'director']
    list_filter=['anio']
    list_per_page=10

admin.site.register(Pelicula)
admin.site.register(Profile)