from django.urls import path, include
from . import views
from .views import PeliculaViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register('peliculas', PeliculaViewSet)

urlpatterns = [
    path('', views.index, name="index"),
    path('memento/', views.memento, name="memento"),
    path('matrix/', views.matrix, name="matrix"),
    path('requiem/', views.requiem, name="requiem"),
    path('pulp-fiction/', views.pulp_fiction, name="pulp-fiction"),
    path('listado-peliculas/', views.listado_peliculas, name="listado-peliculas"),
    path('agregar-pelicula/', views.agregar_pelicula, name="agregar-pelicula"),
    path('modificar-pelicula/<id>/', views.modificar_pelicula, name="modificar-pelicula"),
    path('eliminar-pelicula/<id>/', views.eliminar_pelicula, name="eliminar-pelicula"),
    path('eliminar-pelicula-recomendaciones/<id>/', views.eliminar_pelicula_recomendaciones, name="eliminar-pelicula-recomendaciones"),
    path('recomendaciones/', views.visualizar_recomendaciones, name="recomendaciones"),
    path('registro-usuario/', views.registrar_usuario, name="registro-usuario"),
    path('perfil/', views.perfil, name="perfil"),
    path('api/', include(router.urls)), #url de la api -> localhost:8000/api/peliculas
]