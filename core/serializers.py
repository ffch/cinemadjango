#serializador en django:
#entrega los datos desde la bd en formato JSON
# bbdd -> datos -> JSON
# y viceversa
# JSON -> datos -> bbdd

from rest_framework import serializers
from .models import Pelicula

class PeliculaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pelicula
        fields=['titulo', 'anio', 'duracion', 'pais', 'director', 'sinopsis']